import AppBar from '@material-ui/core/AppBar';
import FlexGrow from './FlexGrow'
import IconButton from '@material-ui/core/IconButton';
import React from 'react';
import Spacer from './Spacer'
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        alignItems: 'center',
    },
    grow: {
        flexGrow: 1,
    },
    icon: {
        marginRight: theme.spacing(1),
        marginTop: theme.spacing(0.5),
        color: theme.palette.primary.light,
    },
}));

const Comp = function (props) {

    const { title, leftAction, rightAction, titleVariant = 'h6' } = props;

    const classes = useStyles();

    const Left = () => {
        if (!leftAction) return <Spacer />

        return (
            <IconButton edge="start" color="inherit" onClick={() => leftAction.action()}>
                {leftAction.icon}
            </IconButton>
        )
    }

    const Right = () => {
        if (!rightAction) return <Spacer />

        return (
            <IconButton edge="end" color="inherit" onClick={() => rightAction.action()}>
                {rightAction.icon}
            </IconButton>
        )
    }

    return (
        <AppBar position="sticky">
            <Toolbar className={classes.root}>
                <Left />
                <FlexGrow />
                <Typography variant={titleVariant} color="inherit">{title}</Typography>
                <FlexGrow />
                <Right />
            </Toolbar>
        </AppBar>
    );
}

export default Comp;
