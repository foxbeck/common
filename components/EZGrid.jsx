import Grid from '@material-ui/core/Grid';
import React from "react";
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    spacing: {
        paddingRight: theme.spacing(1),
    },

}));

export default function Comp({ children }) {
    const classes = useStyles();

    return (
        <div>
        <Grid container spacing={1} direction='row' alignItems="center">
            {children.map((child, idx) => (
                <Grid key={idx} item>
                    {child}
                </Grid>
            ))}
        </Grid>
        </div>
    )
}