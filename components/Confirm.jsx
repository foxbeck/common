import React, { useState } from 'react';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';

export default function Comp({ action, button }) {

    const [open, setOpen] = useState(false);

    const confirmed = () => {
        action()
        handleClose()
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <>
            {React.cloneElement(button, { onClick: () => setOpen(true) })}

            <Dialog open={open} onClose={handleClose} >
                <DialogTitle >Please Confirm...</DialogTitle>
                <DialogActions>
                    <Button variant='contained' onClick={(handleClose)} color="default">
                        Cancel
                    </Button>
                    <Button variant='contained' onClick={confirmed} color="primary" autoFocus >
                        Confirm
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
}
