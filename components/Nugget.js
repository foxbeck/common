import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import React from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
	root: {
		padding: theme.spacing(2),
	},
	paper: {
		background: theme.palette.primary.main,
		color: theme.palette.primary.contrastText,
	}
}));

export default function Comp({ context, headline, description='' }) {

	const classes = useStyles();

	return (
		<Paper className={classes.paper}>
			<Grid container spacing={0} className={classes.root}>
				<Grid item xs={12}>
					<Typography align='center' variant='h6' component='div'>{context}</Typography>
				</Grid>
				<Grid item xs={12}>
					<Typography align='center' variant='h3'>{headline}</Typography>
				</Grid>
				<Grid item xs={12}>
					<Typography align='center' variant='caption' component='div'>{description}</Typography>
				</Grid>
			</Grid>
		</Paper>
	);
}