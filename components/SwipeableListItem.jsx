import {
  Box,
  Grid,
  ListItem,
  makeStyles,
} from '@material-ui/core';
import React, { useRef, useState } from 'react';

import { Delete } from '@material-ui/icons';

const NONE = 'none'
const HORZ = 'horz'
const VERT = 'vert'

const useStyles = makeStyles((theme) => ({
  backgroundClass: {
    position: `absolute`,
    width: `100%`,
    height: `100%`,
    // zIndex: -1,
    color: `#fff`,
    backgroundColor: theme.palette.background.default,
    padding: 0,
    margin: 0,
  },
  listItemClass: {
    backgroundColor: theme.palette.background.default,
    // transition: `transform 0.3s ease-out`,
  },
  wrapperClass: {
    position: `relative`,
    transition: `max-height 0.5s ease`,
    transformOrigin: `top`,
    overflow: `hidden`,
    width: `100%`,
  },
}));

const SwipeableListItem = ({
  divider,
  button,
  onClick,
  children,
  disableDeleteAnimation = true,
  ListItemProps = {},
  threshold = 0.2,
  deleteAction,
  actions = [],
  actionWidth = 70,
}) => {
  const classes = useStyles();
  const { backgroundClass, listItemClass, wrapperClass } = classes;
  const listElementEl = useRef(document.createElement(`li`));
  const [state, setState] = useState({
    wrapperMaxHeight: 1000,
    diff: 0,
    dragged: false,
    dragStartX: 0,
    dragStartY: 0,
    side: `none`,
    startTime: 0,
    disableClick: false,
  });
  const [swipeDirection, setSwipeDirection] = useState(NONE)

  const { className, ...restOfListItemProps } = ListItemProps;

  const { diff, dragged, dragStartX, dragStartY, side, wrapperMaxHeight, disableClick } = state
  const absDiff = Math.abs(diff)

  function onDragStartTouch(event) {
    console.log('start swipe',diff);

    const touch = event.touches[0];
    const { clientX, clientY } = touch;
    setState((prevState) => ({
      ...prevState,
      dragged: true,
      dragStartX: clientX,
      dragStartY: clientY,
      startTime: Date.now(),
      diff: 0,
      disableClick: diff !== 0,
    }));
    setSwipeDirection(NONE)

  }

  function onDragEndTouch() {
    setSwipeDirection(NONE)

    console.log('end swipe',diff);
    if (dragged) {

      setState((prevState) => ({
        ...prevState,
        dragged: false,
      }));

      if (diff < listElementEl.current.offsetWidth * threshold * -1) {
        setState((prevState) => ({
          ...prevState,
          diff: -actionWidth,
          wrapperMaxHeight: 0,
        }));
      }
      else if (diff > listElementEl.current.offsetWidth * threshold) {
        setState((prevState) => ({
          ...prevState,
          diff: actionWidth * actions.length,
        }));
      }
      else {
        setState((prevState) => ({ ...prevState, diff: 0 }));
      }
    }
  }

  function onTouchMove(event) {
    const touch = event.touches[0]
    
    if (swipeDirection === VERT) return
    
    const newDiff = touch.clientX - dragStartX
    
    if (swipeDirection === NONE) {
      
      const newDiffY = touch.clientY - dragStartY
      const dir = Math.abs(newDiff) > Math.abs(newDiffY) ? HORZ : VERT
      setSwipeDirection(dir)
      
      if (dir === VERT) return
    }

    
    if (newDiff < 0 && deleteAction) {
      setState((prevState) => ({
        ...prevState,
        diff: newDiff,
        side: `left`,
      }));
    } else if (newDiff > 0 && actions.length > 0) {
      setState((prevState) => ({
        ...prevState,
        diff: newDiff,
        side: `right`,
      }));
    }
  }

  function onTransitionEnd(event) {
    event.persist();
    const { propertyName } = event;
    const propertyCheck = disableDeleteAnimation || propertyName === `max-height`;
    if (side === `left` && propertyCheck && !dragged && diff < listElementEl.current.offsetWidth * threshold * -1) {
      onSwipedLeft(event);
      if (disableDeleteAnimation)
        setState((prevState) => ({
          ...prevState,
          diff: 0,
        }));
    } else if (side === `right` && !dragged && diff > listElementEl.current.offsetWidth * threshold) {
      onSwipedRight(event);
      setState((prevState) => ({
        ...prevState,
        diff: 0,
      }));
    } else {
      setState((prevState) => ({
        ...prevState,
      }));
    }
  }

  function onActionClick(action) {
    console.log('on click');

    setState((prevState) => ({
      ...prevState,
      diff: 0,
    }))

    action()
  }

  function onListItemClick() {
    console.log('on list item click', disableClick);

    onClick && !disableClick && onClick()
  }

  const getOpacity = () => {
    const opacity = parseFloat(absDiff / 100).toFixed(2)
    if (opacity < 1) {
      return opacity;
    }
    return 1;
  };

  const Action = ({ config }) => (
    <Grid item xs
      style={{
        backgroundColor: config.background,
        opacity: getOpacity(),
      }}   >
      <Box
        height='100%'
        display="flex"
        alignItems="center"
        justifyContent="center"
        onClick={() => onActionClick(config.action)}

      >
        {config.icon}

      </Box>
    </Grid>

  )

  const DeleteAction = () => (
    <>
      <Grid item xs></Grid>
      <Grid item container
        justifyContent='space-around'
        alignItems='stretch'
        style={{
          width: absDiff,
          height: '100%',
        }}   >
        <Grid item xs
          style={{
            backgroundColor: 'red',
            opacity: getOpacity(),
          }}
        >
          <Box
            height='100%'
            display="flex"
            alignItems="center"
            justifyContent="center"
            onClick={() => onActionClick(deleteAction)}
          >
            <Delete />
          </Box>
        </Grid>
      </Grid>
    </>

  )

  const OtherActions = () => (
    <>
      <Grid item container
        justifyContent='space-around'
        alignItems='stretch'
        wrap='nowrap'
        style={{
          width: absDiff,
          height: '100%',
        }}   >
        {actions.map(action => (
          <Action key={action.id} config={action} />
        ))}

      </Grid>
      <Grid item xs></Grid>

    </>

  )

  return (
    <div
      className={wrapperClass}
      data-testid="wrapper-list-item"
      // onTransitionEnd={onTransitionEnd}
      style={{
        maxHeight: !disableDeleteAnimation ? wrapperMaxHeight : undefined,
      }}

    >
      <ListItem
        className={backgroundClass}
        data-testid="action-list-item"
        divider={dragged}

      >

        <Grid container
          style={{
            width: '100%',
            height: '100%',
          }}
        >
          {side === 'left' ? <DeleteAction /> : <OtherActions />}

        </Grid>
      </ListItem>
      <ListItem
        {...restOfListItemProps} // eslint-disable-line react/jsx-props-no-spreading
        className={[listItemClass, className].join(` `)}
        data-testid="draggable-list-item"
        divider={divider || dragged}
        onTouchStart={onDragStartTouch}
        onTouchMove={onTouchMove}
        onTouchEnd={onDragEndTouch}
        onClick={onListItemClick}
        button={button}
        ref={listElementEl}
        style={{
          transform: `translateX(${diff}px)`,
        }}
      >
        {children}
      </ListItem>
    </div>
  )
}

export default SwipeableListItem