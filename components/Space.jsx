import Grid from '@material-ui/core/Grid';
import React from "react";
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    spacing: {
        paddingRight: theme.spacing(1),
    },

}));

export default function Comp({ children }) {
    const classes = useStyles();

    return (
        <>
            {children.map((child, idx) => (
                <div key={idx} className={classes.spacing}>
                    {child}
                </div>
            ))}
        </>
    )
}