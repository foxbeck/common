import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import React from 'react';
import Typography from '@material-ui/core/Typography';

export default function Comp({ open, setOpen, action }) {

    const confirmed = () => {
        action()
        handleClose()
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (

        <Dialog open={open} onClose={handleClose} >
            <DialogTitle >Please Confirm...</DialogTitle>
            {/* <DialogContent>
                <Typography>Please confirm</Typography>
            </DialogContent> */}
            <DialogActions>
                <Button variant='contained' onClick={(handleClose)} color="default">
                    Cancel
                </Button>
                <Button variant='contained' onClick={confirmed} color="primary" autoFocus >
                    Confirm
                </Button>
            </DialogActions>
        </Dialog>
    );
}
