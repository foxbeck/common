import IconButton from '@material-ui/core/IconButton';
import Popover from '@material-ui/core/Popover';
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({

    openButton: {
        marginLeft: theme.spacing(2),
    },

}));

export default function Comp(props) {
    const classes = useStyles();

    const { icon, actions, direction='right' } = props;
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);

    const handleClick = (event) => {
        event.stopPropagation();
        setAnchorEl(event.currentTarget);
    };

    const handleClose = (event) => {
        event.stopPropagation();
        setAnchorEl(null);
    };

    const handleAction = (event, action) => {
        event.stopPropagation();
        setAnchorEl(null);
        action();
    };

    return (
        <div>
            <IconButton onClick={handleClick} size='small'>
                {icon}
            </IconButton>
            <Popover
                open={open}
                PaperProps={{ elevation: 0 }}
                anchorEl={anchorEl}
                onClose={handleClose}
                onClick={handleClose}
                anchorOrigin={{
                    vertical: 'center',
                    horizontal: direction,
                }}
                transformOrigin={{
                    vertical: 'center',
                    horizontal: direction,
                }}
            >

                {actions.map(action => (
                    <IconButton className={classes.openButton} size='small' key={action.name} onClick={(event) => handleAction(event, action.onClick)} disabled={!open}>
                        {action.icon}
                    </IconButton>
                ))}


            </Popover>
        </div>
    );
}
