import React, { useState } from 'react';

import List from '@material-ui/core/List';

const NONE = 'none'
const HORZ = 'horz'
const VERT = 'vert'

export default function SwipeableList({ children }) {
    const [start, setStart] = useState({ x: 0, y: 0 })
    const [swipeDirection, setSwipeDirection] = useState(NONE)

    const onTouchStart = (event) => {
        const touch = event.touches[0];
        setStart({ x: touch.clientX, y: touch.clientY })
        setSwipeDirection(NONE)
        console.log('start')
    }

    const onTouchMove = (event) => {
        if (swipeDirection !== NONE) return

        const touch = event.touches[0];
        const diffX = Math.abs(start.x - touch.clientX)
        const diffY = Math.abs(start.y - touch.clientY)
        const dir = diffX > diffY ? HORZ : VERT
        console.log(dir, diffX, diffY)
        setSwipeDirection(dir)
    }

    const onTouchEnd = (event) => {
        console.log('end')
        setSwipeDirection(NONE)
    }

    return (
        <List
            onTouchStart={onTouchStart}
            onTouchMove={onTouchMove}
            onTouchEnd={onTouchEnd}
            // style={swipeDirection !== NONE ? { touchAction: 'none' } : null }
            // style={{ touchAction: 'none' }}
        >            {children}
        </List>
    )
}