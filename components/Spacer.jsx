import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import React from "react";
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    invisible: {
        color: 'rgba(0,0,0,0)',
    },

}));

export default function Comp({ message, setMessage }) {
    const classes = useStyles();

    return (
        <IconButton>
            <CloseIcon className={classes.invisible} />
        </IconButton>
    );
}