import api from '../../utils/api';

async function fetchResult(path, options) {

	try {
		const ret = await fetch(path, options);
		return ret;

	} catch (err) {
		return null;
	}
}

export async function fetchFromApi(path, options) {
	const res = await fetchResult(api(path), options);
	if (res) {
		return await res.json();
	}
	else {
		return { success: false };
	}
}

async function methodToApi(path, method, payload) {
    let options = {
        method: method,
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(payload)
    };

    return await fetchFromApi(path, options);
}

export async function deleteFromApi(path, payload) {
    return await methodToApi(path, 'delete', payload);
}

export async function putToApi(path, payload) {
    return await methodToApi(path, 'put', payload);
}

export async function postToApi(path, payload) {
    return await methodToApi(path, 'post', payload);
}