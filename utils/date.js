import moment from 'moment';

export const sortByDate = (a,b) => {
    const dateA = moment(a);
    const dateB = moment(b);

    return dateA - dateB;
}
