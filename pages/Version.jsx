import Avatar from '@material-ui/core/Avatar';
import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CloseIcon from '@material-ui/icons/Close';
import Container from '@material-ui/core/Container';
import DateIcon from '@material-ui/icons/EventOutlined';
import Grid from '@material-ui/core/Grid';
import React from 'react';
import TopBar from '../components/TopBar'
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from "react-router-dom";
import version from '../../data/lastcommit.json'

//
// requires client repo to be useing the appropriate pre-commit hook 
// (.git/hooks/pre-commit) 
// and the following client repo file to be writable
// ./src/data/lastcommit.json
//

const useStyles = makeStyles((theme) => ({
    cardGrid: {
        padding: theme.spacing(1),
    },
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    avatar: {
        color: theme.palette.primary.contrastText,
        backgroundColor: theme.palette.primary.light,
    },
   
}));

export default function Comp() {
    const classes = useStyles();
    const history = useHistory();

    const done = {
        icon: <CloseIcon/>,
        action: () => history.push("/")
    }

    return (
        <Box>
            <TopBar title='Version' leftAction={done}/>

            <Container className={classes.cardGrid} maxWidth="sm" >
                <Grid container spacing={5}>
                    <Grid item xs={12} >
                        <Card className={classes.card}>
                            <CardHeader
                                avatar={<Avatar className={classes.avatar}><DateIcon /></Avatar>}
                                title={version.lastCommit}
                                titleTypographyProps={{ variant: 'h5' }}
                            />
                        </Card>
                    </Grid>
                </Grid>
            </Container>

         
        </Box>
    )
}
